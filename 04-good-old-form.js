const express = require('express');
const bodyparser = require('body-parser');
const app = express();

const port = process.argv[2];

app.use(bodyparser.urlencoded({extended: false}));

// input form:
// <form><input name="str"/></form>
app.post('/form', (req, res) => {
  const inputString = req.body.str;
  res.end(inputString.split('').reverse().join(''));
});
app.listen(port);
