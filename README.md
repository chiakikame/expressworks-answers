# Notes

* `app.get` for GET route
* `app.use` + `express.static` for static contents
* Using PUG template for rendering
* Process form input with `body-parser`
* Using CSS in express with Stylus
* Processing URL parameters via `req.params` (e.g. `/usr/1a2b3c4d` )
* Processing URL query string via `req.query` (e.g. ?a=3&b=4)

## `res.json` vs `res.send`

http://stackoverflow.com/questions/19041837/difference-between-res-send-and-res-json-in-express-js

`res.json` does additional processing to inputs.
