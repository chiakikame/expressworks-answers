const express = require('express');
const stylus = require('stylus');
const app = express();

const port = process.argv[2];
const assetDir = process.argv[3];

app.use(stylus.middleware(assetDir));
app.use(express.static(assetDir));

app.listen(port);
